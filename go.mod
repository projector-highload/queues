module queues

go 1.17

require (
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/go-redis/redis/v8 v8.11.3
	github.com/paulbellamy/ratecounter v0.2.0
)

require (
	github.com/beanstalkd/go-beanstalk v0.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
)
