package main

import (
	"context"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/paulbellamy/ratecounter"
)

import "github.com/bxcodec/faker/v3"

func main() {
	// c, err := beanstalk.Dial("tcp", "172.22.0.4:11300")
	// if err != nil {
	// 	panic(err)
	// }

	ctx := context.Background()
	rdb := redis.NewClient(&redis.Options{
		Addr:     "172.22.0.3:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	counter := ratecounter.NewRateCounter(1 * time.Second)

	go func() {
		for {
			log.Printf("current rate: %d", counter.Rate())
			time.Sleep(time.Second)
		}
	}()

	ch := make(chan struct{}, 4)

	for {
		ch <- struct{}{}
		go func() {
			if err := rdb.Publish(ctx, "test", faker.Paragraph()).Err(); err != nil {
				panic(err)
			}

			// if _, err := c.Put([]byte(faker.Paragraph()), 1, 0, 120*time.Second); err != nil {
			// 	panic(err)
			// }

			counter.Incr(1)

			<-ch
		}()
	}
}
