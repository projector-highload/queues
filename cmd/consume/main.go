package main

import (
	"context"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/paulbellamy/ratecounter"
)

func main() {
	ctx := context.Background()

	rdb := redis.NewClient(&redis.Options{
		Addr:     "172.22.0.3:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// c, err := beanstalk.Dial("tcp", "172.22.0.4:11300")
	// if err != nil {
	// 	panic(err)
	// }

	counter := ratecounter.NewRateCounter(1 * time.Second)

	go func() {
		for {
			log.Printf("current rate: %d", counter.Rate())
			time.Sleep(time.Second)
		}
	}()

	// for {
	// 	id, _, err := c.Reserve(5 * time.Second)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	c.Delete(id)
	// 	counter.Incr(1)
	// }

	pubsub := rdb.Subscribe(ctx, "test")
	defer pubsub.Close()

	ch := pubsub.Channel()

	for msg := range ch {
		if msg.Payload != "" {
			counter.Incr(1)
		}
	}
}
